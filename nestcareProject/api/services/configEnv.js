var fs = require('fs');
var os = require('os');
var configPath = '';
if(os.platform() === 'win32'){
  configPath = process.env.PWD.replace(/\//g, '\\') + '\\config\\env\\' + process.env.NODE_ENV + '.js';
} else {
  configPath = process.env.PWD + '/config/env/' + process.env.NODE_ENV + '.js';
}
var configEnv = (fs.existsSync(configPath)) ? require(configPath) : undefined;


module.exports = function(){
  return configEnv;
};
