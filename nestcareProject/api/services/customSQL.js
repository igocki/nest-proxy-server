
var configEnv = require('./configEnv')();
var pg = require('pg');
var config = {
  user: configEnv.pgDBUser,
  database: configEnv.pgDatabaseName,
  password: configEnv.pbPass,
  host: configEnv.pgHost,
  port: configEnv.pgPort,
  max: 10,
  idleTimeoutMillis: 30000
};

var pool = new pg.Pool(config);

module.exports = {

  getCompletedOrdersHistory: function(fromDate, toDate,callback){
    pool.connect(function(err, client, done) {
      if(err) {
        console.error('error fetching client from pool', err);
        callback(undefined, err);
        done();
        return true;
      }

      var sql = 'SELECT "id", "completed_orders_id", "user_id", "shippo_id", "tracking_number" as shipservice, "payment_status", "created_at", "updated_at" FROM "public"."completed_orders_history"' +
        ' where created_at::date >= $1::date AND created_at::date <= $2::date';
      console.log(sql);
      var params = [fromDate, toDate];
      client.query(sql, params, function(err, result) {
        //call `done()` to release the client back to the pool
        done();

        if(err) {
          console.error('error running query', err);
          callback(undefined, err);
          done();
          return true;
        }
        console.log(result.rows);
        callback(result.rows);
        return true;
        //output: 1
      });
    });

    pool.on('error', function (err, client) {
      // if an error is encountered by a client while it sits idle in the pool
      // the pool itself will emit an error event with both the error and
      // the client which emitted the original error
      // this is a rare occurrence but can happen if there is a network partition
      // between your application and the database, the database restarts, etc.
      // and so you might want to handle it and at least log it out
      console.error('idle client error', err.message, err.stack)
      return error;
    })
  },

  getUsersPaidFor: function(paying_user,callback){
    pool.connect(function(err, client, done) {
      if(err) {
        console.error('error fetching client from pool', err);
        callback(undefined, err);
        done();
        return true;
      }

      var sql = 'SELECT paid_user_id FROM "public"."paid_for_customers" where user_id = $1';
      var params = [paying_user];
      client.query(sql, params, function(err, result) {
        //call `done()` to release the client back to the pool
        done();

        if(err) {
          console.error('error running query', err);
          callback(undefined, err);
          done();
          return true;
        }
        console.log(result.rows);
        callback(result.rows);
        return true;
        //output: 1
      });
    });

    pool.on('error', function (err, client) {
      // if an error is encountered by a client while it sits idle in the pool
      // the pool itself will emit an error event with both the error and
      // the client which emitted the original error
      // this is a rare occurrence but can happen if there is a network partition
      // between your application and the database, the database restarts, etc.
      // and so you might want to handle it and at least log it out
      console.error('idle client error', err.message, err.stack)
      return error;
    })
  },

  getPayingUser: function(paidUser,callback){
    pool.connect(function(err, client, done) {
      if(err) {
        return console.error('error fetching client from pool', err);
      }
      var sql = 'SELECT user_id FROM "public"."paid_for_customers" where paid_user_id = $1';
      var params = [paidUser];

      client.query(sql, params, function(err, result) {
        //call `done()` to release the client back to the pool
        done();

        if(err) {
          console.error('error running query', err);
          callback(undefined, err);
          return true;
        }
        (result.rows[0]) ? callback(result.rows[0]) : callback({ user_id: null});
        return true;
        //output: 1
      });
    });

    pool.on('error', function (err, client) {
      // if an error is encountered by a client while it sits idle in the pool
      // the pool itself will emit an error event with both the error and
      // the client which emitted the original error
      // this is a rare occurrence but can happen if there is a network partition
      // between your application and the database, the database restarts, etc.
      // and so you might want to handle it and at least log it out
      console.error('idle client error', err.message, err.stack)
      return error;
    })
  },

  insertPaidFor: function(paying_user, paid_for, callback){
    pool.connect(function(err, client, done) {
      if(err) {
        return console.error('error fetching client from pool', err);
      }
      var sql = 'DELETE FROM "public"."paid_for_customers" where user_id = $1 AND paid_user_id = $2';
      var oneparam = [paying_user, paid_for];

      var sqlsecond = 'INSERT INTO "public"."paid_for_customers" ("user_id", "paid_user_id", "created_date") ' +
        'VALUES ($1, $2, current_timestamp)';
      var params = [paying_user, paid_for];
      // var sql = 'INSERT INTO "public"."paid_for_customers" ("user_id", "paid_user_id", "created_date") ' +
      //   'VALUES ($1, $2, current_timestamp)';
      // var params = [paying_user, paid_for];

      client.query(sql, oneparam, function(err, result) {
        //call `done()` to release the client back to the pool

        if(err) {
          console.error('error running query', err);
          callback(undefined, err);
          return true;
        }
        console.log({ msg: "success"});

        client.query(sqlsecond, params, function(err, result) {
          console.log({ msg: "success23"});
          //call `done()` to release the client back to the pool
          done();
          if(err) {
            console.error('error running query', err);
            callback(undefined, err);
            return true;
          }
          console.log({ msg: "success"});
          callback({ msg: "success"});
          return true;
          //output: 1
        });
        return true;
        //output: 1
      });


    });

    pool.on('error', function (err, client) {
      // if an error is encountered by a client while it sits idle in the pool
      // the pool itself will emit an error event with both the error and
      // the client which emitted the original error
      // this is a rare occurrence but can happen if there is a network partition
      // between your application and the database, the database restarts, etc.
      // and so you might want to handle it and at least log it out
      console.error('idle client error', err.message, err.stack)
      return error;
    })
  },

  deletePaidFor: function(paid_for, callback){
    pool.connect(function(err, client, done) {
      if(err) {
        return console.error('error fetching client from pool', err);
      }

      var sql = 'DELETE FROM "public"."paid_for_customers" where paid_user_id = $1';
      var params = [paid_for];

      client.query(sql, params, function(err, result) {
        //call `done()` to release the client back to the pool
        done();

        if(err) {
          console.error('error running query', err);
          callback(undefined, err);
          return true;
        }
        console.log({ msg: "success"});
        callback({ msg: "success"});
        return true;
        //output: 1
      });
    });

    pool.on('error', function (err, client) {
      // if an error is encountered by a client while it sits idle in the pool
      // the pool itself will emit an error event with both the error and
      // the client which emitted the original error
      // this is a rare occurrence but can happen if there is a network partition
      // between your application and the database, the database restarts, etc.
      // and so you might want to handle it and at least log it out
      console.error('idle client error', err.message, err.stack)
      return error;
    })
  },

  changeAccountType: function(user_id,accountTo, callback){
    console.log('helklooo');
    pool.connect(function(err, client, done) {
      if(err) {
        return console.error('error fetching client from pool', err);
      }

      var sql = 'DELETE FROM "public"."user_account_type" where user_id = $1';
      var oneparam = [user_id];
      var params = [user_id, accountTo];
      var sqlsecond = 'INSERT INTO "public"."user_account_type" ("user_id", "account_type_id", "status_id", "created_at", "updated_at") ' +
        'VALUES ($1, $2, 1, current_timestamp, current_timestamp)';
      client.query(sql, oneparam, function(err, result) {
        console.log({ msg: "success1"});
        //call `done()` to release the client back to the pool
        if(err) {
          console.error('error running query', err);
          callback(undefined, err);
          return true;
        }

        client.query(sqlsecond, params, function(err, result) {
          console.log({ msg: "success23"});
          //call `done()` to release the client back to the pool
          done();
          if(err) {
            console.error('error running query', err);
            callback(undefined, err);
            return true;
          }
          console.log({ msg: "success"});
          callback({ msg: "success"});
          return true;
          //output: 1
        });
        return true;
        //output: 1
      });


    });

    pool.on('error', function (err, client) {
      // if an error is encountered by a client while it sits idle in the pool
      // the pool itself will emit an error event with both the error and
      // the client which emitted the original error
      // this is a rare occurrence but can happen if there is a network partition
      // between your application and the database, the database restarts, etc.
      // and so you might want to handle it and at least log it out
      console.error('idle client error', err.message, err.stack)
      return error;
    })
  }



};
