/**
 * Created by Tommy on 2/10/2017.
 */

var request = require('request');


module.exports = {
  api_key:'8deda345482ccd260eaf8c01c40d73e3df04b5061025f24ec08d2350212d0303',
  api_user:'tommy',
  api_base:'http://community.mynest.care',


//  users/by-external/:external_id
  ssoLogout: function(req, res) {
    var params = req.allParams();
    var payload = params['userId'];
    var params = {
      'api_key': this.api_key,
      'api_user': this.api_user
    };
    var derp = this;

    this.get('/users/by-external/' + payload + '.json', params, function(data, error) {

      if(error){
        return res.json(error);
      }

      derp.post('/admin/users/' + data.user.id + '/log_out', {}, function(data, error) {
        if (error) {
          return res.json(error);
        } else {
          return res.json(data);
        }
      });



    });


  },

  get: function(endpoint, params, callback) {
    //var headers = {'api_key': this.api_key, 'api_username': this.api_user};

    var options = {
      url: endpoint,
      baseUrl: this.api_base,
     // headers: headers,
      qs: params
    };

    request(options, function (error, response, body) {
      if (!error && response.statusCode == 200) {
        var info = JSON.parse(body);
        return callback(info);
      } else {
        if(response.statusCode === 404){
          var infoError = {
            message: {
              message: 'No discourse login found'
            }
          };
          return callback(undefined,infoError);
        } else {
          var infoError = JSON.parse(body);
          console.log('Request Error (POST): ' + error);
          console.log('Request Error (RESPONSE): ' + response);
          console.log('Request Error (BODY): ' + body);
          var infoError = {
            message: JSON.parse(body)
          };
          return callback(undefined,infoError);
        }

      }
    });
  },

  post: function(endpoint, params, callback) {
    var headers = {'api_key': this.api_key, 'api_username': this.api_user};

    var options = {
      url: endpoint,
      baseUrl: this.api_base,
   //   headers: headers,
      method: 'POST',
      form: headers
    };
    console.log('Requesting ' + endpoint + ' -- ' + this.api_base);

    request(options, function(error, response, body) {
      if (!error && response.statusCode == 200) {
        var info = JSON.parse(body);
        return callback(info);
      } else {
        console.log('Request Error (POST): ' + error);
        console.log('Request Error (RESPONSE): ' + response);
        console.log('Request Error (BODY): ' + body);
        var infoError = JSON.parse(body);

        var infoError = {
          message: JSON.parse(body)
        };
        return callback(undefined,infoError);
      }
    });
  }
};
