/**
 * ShippoController
 *
 * @description :: Server-side logic for managing the GoShippo API.
 * @help        :: See http://sailsjs.org/#!/documentation/concepts/Controllers
 */

var configEnv = require('../services/configEnv')();
var request = require ('request');
var twilio = require('twilio');

// Find your account sid and auth token in your Twilio account Console.
var client = twilio(configEnv.twilioSID, configEnv.twilioAuth);

// Send the text message.


module.exports = {

  sendRandomNumberVerify: function(req, res) {

    var ranNumb = Math.floor(Math.random() * (Math.floor(100000) - Math.ceil(10000))) + Math.ceil(10000);

    client.sendMessage({
      to: req.param('phone'),
      from: configEnv.twilioPhoneNumber,
      body: 'Your nestcare authorization number is: ' +  ranNumb
    }, function(err, data){
      console.log('errror: ' + err);
      console.log("data + " + data);
      if(err){
        return res.status(422).send({
          message: {
            message: 'Verify Could not send.'
          }
        })
      } else {
        return res.json({
          message: 'success',
          randomNumber: ranNumb,
          data: data
        });
      }
    });


  },
};

/**
 * Created by Andrew on 2/10/2017.
 */
