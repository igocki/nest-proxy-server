/**
 * PayController
 *
 * @description :: Server-side logic for managing Paywhirl, Mirrors Paywhirl's PHP API.
 * @help        :: See http://sailsjs.org/#!/documentation/concepts/Controllers
 */

  // -- Data flows in from outside ->
  // -- Requestjs sends request out
  // -- Data comes back, Requestjs calls its callback.
  // -- Requestjs callback calls OUR callback
  // -- Our callback forwards data back via sails, request complete.
  // -- Add more routes as they become needed.
var request = require('request');
var jwt = require('jwt-simple');
var configEnv = require('../services/configEnv')();
var Recurly = require('node-recurly');
var recurly = new Recurly(configEnv);
var Promise = require("bluebird");
var pgQuery = require('../services/customSQL');
module.exports = {
	api_base: 'https://api.paywhirl.com',
  api_key: configEnv.paywhirlkey,
  api_secret: configEnv.paywhirlsecret,

  getActiveCustomers: function(req, res) {
    recurly.accounts.list({}, function(error, dt){
      console.log('this came back with');
      console.log('error: ' + error);
      console.log('data: ' + dt);
      if(error) {
        if(error.statusCode === 503){
          return res.status(503).send({
            message: 'Recurly servers down'
          });
        }

        return res.negotiate(error);
      }
      var customerList = [];

      if(dt.data.accounts.account instanceof Array){
        dt.data.accounts.account.forEach(function(acct){
          if(acct.state === 'active'){
            customerList.push({
              account_code: acct.account_code,
              email: acct.email,
              first_name: acct.first_name,
              last_name: acct.last_name,
              shipping_address: {
                address1: acct.address.address1,
                address2: acct.address.address2,
                city: acct.address.city,
                state: acct.address.state,
                zip: acct.address.zip
              },
              phone: acct.address.phone,
              created: acct.created_at._
            })
          }
        });
      } else {
        var item = dt.data.accounts.account;
        if(item.state === 'active') {
          customerList.push({
            account_code: item.account_code,
            email: item.email,
            first_name: item.first_name,
            last_name: item.last_name,
            shipping_address: {
              address1: item.address.address1,
              address2: item.address.address2,
              city: item.address.city,
              state: item.address.state,
              zip: item.address.zip
            },
            phone: item.address.phone,
            created: item.created_at._
          })
        }
      }


      return res.json(customerList);
    });
  },

  getAllSubscribedCustomers: function(req, res) {
    recurly.accounts.list({state: 'subscriber'}, function(error, dt){
      console.log('this came back with');
      console.log('error: ' + error);
      console.log('data: ' + dt);
      // console.log(JSON.stringify(dt));
      if(error) {
        if(error.statusCode === 503){
          return res.status(503).send({
            message: 'Recurly servers down'
          });
        }
        return res.negotiate(error);
      }
      var customerList = [];

      if(dt.data.accounts.account instanceof Array){
        dt.data.accounts.account.forEach(function(acct){
            customerList.push({
              account_code: acct.account_code,
              email: acct.email,
              first_name: acct.first_name,
              last_name: acct.last_name,
              shipping_address: {
                address1: acct.address.address1,
                address2: acct.address.address2,
                city: acct.address.city,
                state: acct.address.state,
                zip: acct.address.zip
              },
              phone: acct.address.phone,
              created: acct.created_at._
            })
          });
      } else {
        var item = dt.data.accounts.account;
        customerList.push({
          account_code: item.account_code,
          email: item.email,
          first_name: item.first_name,
          last_name: item.last_name,
          shipping_address: {
            address1: item.address.address1,
            address2: item.address.address2,
            city: item.address.city,
            state: item.address.state,
            zip: item.address.zip
          },
          phone: item.address.phone,
          created: item.created_at._
        })
      }


      return res.json(customerList);
    });
  },

  getInvoiceSecCheck: function(req, res){
    var payload = {
      sub: req.param('id')
    };
    var tok = jwt.encode(payload, configEnv.sailssecretTemp);
	  return res.json({success: true, token: tok});
  },

  getInvoicePdf: function(req, res) {

    if(!req.param('temp')){
      return res.forbidden();
    }
    var payload = jwt.decode(req.param('temp'), configEnv.sailssecretTemp);
    if((!payload.sub || payload.sub !== req.params.all().id + '')){
      return res.forbidden();
    }

    pgQuery.getUsersPaidFor(req.param('id'),function(data, error) {
      if (error) {
        return res.status(422).send({
          message: {
            message: 'Could not get invoice'
          }
        })
      }

      if(data){

        var invoiceGet = Promise.promisify(recurly.invoices.get);


        var idArray = data.map(function(item){
          return item.paid_user_id;
        })
        var resultInvoices = getInvoiceListPrivate(idArray, req.param('id'));


        return resultInvoices.then(function(invoiceList){
          var invoices = invoiceList.map(function(item){
            return item.invoice_number;
          });
          console.log(invoices);
          if(invoices.indexOf(req.param("invoice_id")) > -1) {
            console.log('this this');
            return invoiceGet(req.param("invoice_id")).then(function (dt) {
              res.setHeader('Content-Type', 'application/pdf');
              res.write(dt.data);
              res.end(dt.data);
            }).catch(function (e) {
              console.log(e);
              return res.negotiate(e);
            });
          }
          else {
            console.log('sent error');
            return res.status(404).send({
              message: 'No Invoice found.'
            });
          }
        }).error(function(e){
          console.log(e);
          return res.status(404).send({
            message: 'No Invoice found.'
          });
        });
      } else {
        return res.status(404).send({
          message: 'No Invoice found.'
        });
      }

    });



	 //  recurly.invoices.get(req.param("invoice_id"), function (error, dt) {
    //
    // });
  },

  getCollectedInvoices: function(req, res) {
    console.log('did this')
    pgQuery.getUsersPaidFor(req.param('id'),function(data, error){
      if(error){
        return res.status(422).send({
          message: {
            message: 'Could not retrieve Paid Users'
          }
        })
      }
      console.log('came bakc with');
      console.log(data);
      if(data){
        var idArray = data.map(function(item){
          return item.paid_user_id;
        })
        console.log('Hehrer');
        console.log(req.param('household'));
        if(req.param('household')){
          idArray = [];
        }
        var resultInvoices = getInvoiceListPrivate(idArray, req.param('id'));
        resultInvoices.then(function(invoices){
          return res.json(invoices);
        }).error(function(e){
          console.log(e);
          return res.status(422).send({
            message: {
              message: 'Could not retrieve Invoices'
            }
          });
        });
      } else {
        return res.json([]);
      }
      // return res.json(data);
    });

    // recurly.invoices.listByAccount(req.param("id"), {state: 'collected'}, function(error, dt){
    //   console.log('this came back with');
    //   console.log('error: ' + error);
    //   console.log('data: ' + dt);
    //   if(error) {
    //     if(error.statusCode === 503){
    //       return res.status(503).send({
    //         message: 'Recurly servers down'
    //       });
    //     }
    //     return res.negotiate(error);
    //   }
    //
    //   var invoiceList = [];
    //
    //   if(dt.data.invoices.invoice instanceof Array){
    //     dt.data.invoices.invoice.forEach(function(invoice){
    //       invoiceList.push({
    //         invoice_number: invoice.invoice_number._,
    //         closed_date: invoice.closed_at._,
    //         amt: invoice.total_in_cents._ / 100
    //       })
    //     });
    //   } else {
    //     if(dt.data && dt.data.invoices && dt.data.invoices.invoice){
    //       var item = dt.data.invoices.invoice;
    //       invoiceList.push({
    //         invoice_number: item.invoice_number._,
    //         closed_date: item.closed_at._,
    //         amt: item.total_in_cents._ / 100
    //       })
    //     }
    //
    //   }
    //
    //   return res.json(invoiceList);
    // });
  },

  getActiveSubscriptions: function(req, res) {

    recurly.subscriptions.listByAccount(req.param("id"), {state: 'active'}, function(error, dt){
      console.log('this came back with');
      console.log('error: ' + error);
      console.log('data: ' + dt);
      if(error) {
        if(error.statusCode === 503){
          return res.status(503).send({
            message: 'Recurly servers down'
          });
        }
        return res.negotiate(error);
      }
      var subscriptionObj = {};
      if(dt.data.subscriptions.subscription instanceof Array){
        var item = dt.data.subscriptions.subscription[0];
        subscriptionObj = {
          collection_method: item.collection_method,
          last_payment: item.current_period_started_at._,
          next_payment: item.current_period_ends_at._,
          planName: item.plan.name,
          planCode: item.plan.plan_code,
          planCost: item.unit_amount_in_cents._ / 100,
          state: item.state,
          multipleActiveSubscriptions: true
        }
      } else {
        if(dt.data.subscriptions.subscription){
          subscriptionObj = {
            collection_method: dt.data.subscriptions.subscription.collection_method,
            last_payment: dt.data.subscriptions.subscription.current_period_started_at._,
            next_payment: dt.data.subscriptions.subscription.current_period_ends_at._,
            planName: dt.data.subscriptions.subscription.plan.name,
            planCode: dt.data.subscriptions.subscription.plan.plan_code,
            planCost: dt.data.subscriptions.subscription.unit_amount_in_cents._ / 100,
            state: dt.data.subscriptions.subscription.state,
            multipleActiveSubscriptions: false
          }
        }
      }

      return res.json(subscriptionObj);
    });
  },

  getCustomer: function(req, res) {

    recurly.accounts.get(req.param("id"), function(error, dt){
      console.log('this came back with');
      console.log('error: ' + error);
      console.log('data: ' + dt);
      if(error) {
        if(error.statusCode === 503){
          return res.status(503).send({
            message: 'Recurly servers down'
          });
        }
        if(error.statusCode === 400){
          return res.status(400).send({
            message: 'Recurly Customer Not Found'
          });
        }
        if(error.statusCode === 404){
          return res.status(404).send({
            message: 'Recurly Customer Not Found'
          });
        }
        console.log(error);
        return res.negotiate(error);
      }

      var customerObj = {
        email: dt.data.account.email,
        first_name: dt.data.account.first_name,
        last_name: dt.data.account.last_name,
        state: dt.data.account.state,
        account_code: dt.data.account.account_code
      };


      return res.json(customerObj);
    });
  },

  refundCharge: function(req, res) {

    recurly.adjustments.remove(req.param('uuid'), function(error, dt){
      if(error) {
        return res.status(422).send({
          message: 'Could not delete adjustment'
        });
      }

      console.log('success');
      console.log(dt);
      return res.json(dt.data);
    });


  },

  createCustomer: function(req, res) {
    var parameters = req.allParams();
    console.log(parameters);
      var postData = {
        account_code: parameters.nc_id,
        email: parameters.email,
        first_name: parameters.first_name,
        last_name: parameters.last_name
      };
      recurly.accounts.create(postData, function(error, dt){
        console.log('this came back with');
        console.log('error: ' + error);
        console.log('data: ' + dt);
        if(error) {
          if(error.statusCode === 503){
            return res.status(503).send({
              message: 'Recurly servers down'
            });
          }
          if(error.data && error.data.errors &&
            error.data.errors.error._ === 'has already been taken'){
            return res.status(422).send({
              message: 'Email address already in use.'
            });
          }
          return res.negotiate(error);
        }
        var payload = {
          sub: dt.data.account.account_code,
          payment_source_id: req.param('payment_source_id')
        };
        dt.data.token = jwt.encode(payload, configEnv.sailssecret);
        return res.json(dt.data);
      });
      // this.post('/create/customer', parameters, function(data, error) {

      //

      //
      //   return res.json(data);
      // });

  },

  updateCustomer: function(req, res) {
    var token = req.headers.authorizationig.split(' ')[1];
    var payload = jwt.decode(token, configEnv.sailssecret);
    var customParams = req.allParams();
    customParams.password = payload.pw;
    this.post('/update/customer', customParams, function(data, error) {

      if(error){

        return res.negotiate(error);
      }

      return res.json(data);
    });
  },

  updateAnswer: function(req, res) {
    this.post('/update/answer', req.allParams(), function(data) {
      return res.json(data);
    });
  },

  getQuestions: function(req, res) {
    this.get('/questions', req.allParams(), function(data) {
      return res.json(data);
    });
  },

  createCard: function(req, res) {
    var  postData = {
      token_id: req.param("token_id")
    };
    console.log('==============CREATE CARD');
    console.log(req.allParams());
    recurly.billingInfo.create(req.param("id"), postData, function(error, dt){
      console.log('this came back with');
      console.log('error: ' + error);
      console.log('data: ' + dt);
      if(error) {
        if(error.statusCode === 503){
          return res.status(503).send({
            message: 'Recurly servers down'
          });
        }
        return res.negotiate(error);
      }
      return res.json(dt.data);
    })
  },

  createCharge: function(req, res) {

    var  postData = {
      description: req.param("description"),
      unit_amount_in_cents: parseFloat(req.param("amount")) * 100,
      currency: "USD",
      quantity: (req.param('quantity')) ? req.param('quantity') : 1
    };
    console.log(postData);
    recurly.adjustments.create( req.param("id") ,postData, function(error, dt){
      console.log('this came back with');
      console.log('error: ' + error);
      console.log('data: ' + dt);
      if(error) {
        if(error.statusCode === 503){
          return res.status(503).send({
            message: 'Recurly servers down'
          });
        }
        return res.negotiate(error);
      }
      return res.json(dt.data);
    })
  },

  deleteCard: function(req, res) {
    var parameters = {
      id: req.allParams().cardID
    };

    recurly.billingInfo.remove(req.param('id'), function(error, dt){
      console.log('this came back with');
      console.log('error: ' + error);
      console.log('data: ' + dt);
      if(error && error.data && error.data.error.symbol !== 'not_found') {
        return res.negotiate(error);
      } else {
        if(error && error.statusCode === 503){
          return res.status(503).send({
            message: 'Recurly servers down'
          });
        }
        return res.json({});
      }
      return res.json(dt.data);
    });
  },

  getCard: function(req, res) {
    this.get('/card/' + req.param('id'), [], function(data, error) {
      if(error){
        return res.negotiate(error);
      }

      return res.json(data);
    });
  },

  getCards: function(req, res) {
    var idToSend = req.param('id');
    recurly.billingInfo.get(req.param('id'), function(error, dt){
      console.log('this came back with');
      console.log('error: ' + error);
      console.log('data: ' + dt);
      if(error) {
        if(error.statusCode === 503){
          return res.status(503).send({
            message: 'Recurly servers down'
          });
        }
        if(error && error.statusCode === 404 && error.data.error &&
          error.data.error.description &&
          error.data.error.description._ === "Couldn't find BillingInfo with account_code = " + idToSend){
          return res.json([]);
        }
        return res.negotiate(error);
      }

      return res.json(dt.data);
    });
  },

  getPlans: function(req, res) {
    recurly.plans.list({}, function(error, dt){
      console.log('this came back with');
      console.log('error: ' + error);
      console.log('data: ' + dt);
      var planList = [];
      if(error) {
        if(error.statusCode === 503){
          return res.status(503).send({
            message: 'Recurly servers down'
          });
        }
        return res.negotiate(error);
      }
      if(dt && dt.data && dt.data.plans){
        var planArray = dt.data.plans.plan;
        planArray.forEach(function(pln) {
          var thisplan = {
            plan_code: pln.plan_code,
            name: pln.name,
            description: pln.description,
            accounting_code: pln.accounting_code,
            amount: parseFloat(pln.unit_amount_in_cents.USD._) / 100,
            setupFee: parseFloat(pln.setup_fee_in_cents.USD._) / 100
          }
          if(thisplan.plan_code.indexOf('main') > -1){
            planList.push(thisplan);
          }
        });
        return res.json(planList);
      }

      return res.json(planList);
    });
  },

  getPlan: function(req, res) {
    this.get('/plan/' + req.param('id'), [], function(data) {
      return res.json(data);
    });
  },

  createPlan: function(req, res) {
    this.post('/create/plan', req.allParams(), function(data) {
      return res.json(data);
    });
  },

  updatePlan: function(req, res) {
    this.post('/update/plan', req.allParams(), function(data) {
      return res.json(data);
    });
  },

  getSubscriptions: function(req, res) {
    this.get('/subscriptions/' + req.param('id'), [], function(data) {
      return res.json(data);
    });
  },
// -- TODO: GOSHIPPO API!
  getSubscription: function(req, res) {
    this.get('/subscription/' + req.param('id'), [], function(data) {
      return res.json(data);
    });
  },


  getCoupon: function(req, res) {
    recurly.coupons.get(req.param("coupon_code"), function(error, dt){
      console.log('this came back with');
      console.log('error: ' + error);
      console.log('data: ' + dt);
      if(error) {
        if(error.statusCode === 503){
          return res.status(503).send({
            message: 'Recurly servers down'
          });
        }
        return res.negotiate(error);
      }
      return res.json(dt.data);
    })
  },

  getPromoAmt: function(req, res) {
    recurly.couponRedemption.get(req.param("id"), function(error, dt){
      console.log('this came back with');
      console.log('error: ' + error);
      console.log('data: ' + dt);
      if(error) {
        if(error.statusCode === 503){
          return res.status(503).send({
            message: 'Recurly servers down'
          });
        }
        return res.negotiate(error);
      }
      return res.json(dt.data);
    })
  },

  redeemCoupon: function(req, res) {
    var postData = {
        account_code: req.param("id"),
        currency: "USD"
    };
    recurly.couponRedemption.redeem(req.param('coupon'),postData, function(error, dt){
      console.log('this came back with');
      console.log('error: ' + error);
      console.log('data: ' + dt);
      if(error) {
        if(error.statusCode === 503){
          return res.status(503).send({
            message: 'Recurly servers down'
          });
        }
        return res.negotiate(error);
      }
      return res.json(dt.data);
    })
  },

  subscribeNewCustomer: function(req, res) {
    var  postData = {
      plan_code: req.param("plan_id"),
      account: {
        account_code: req.param("id")
      },
      currency: "USD",
      quantity: (req.param('quantity')) ? req.param('quantity') : 1,
      subscription_add_ons: undefined
    };
    var addons = [];
    if(req.param("setup_fee") === 1){
      addons.push({
        add_on_code: 'addon_setupfee',
        quantity: (req.param('quantity')) ? req.param('quantity') : 1});
    }
    // if(req.param("sec_dep") === 1){
    //   addons.push({add_on_code: 'addon_secdep'});
    // }

    if(addons.length > 0){
      postData.subscription_add_ons = {
        subscription_add_on: addons
      }
    }
    console.log(postData);
    recurly.subscriptions.create(postData, function(error, dt){
      console.log('this came back with');
      console.log('error: ' + error);
      console.log('data: ' + dt);
      if(error) {
        if(error.statusCode === 503){
          return res.status(503).send({
            message: 'Recurly servers down'
          });
        }
        return res.negotiate(error);
      }

      if(dt.data && dt.data.subscription && dt.data.subscription.invoice &&
      dt.data.subscription.invoice.$ && dt.data.subscription.invoice.$.href){
        var urlInvoice = dt.data.subscription.invoice.$.href;
        var invoiceNumber = urlInvoice.substring(urlInvoice.lastIndexOf('/') + 1);
        dt.data.invoiceNum = invoiceNumber;
      }

      return res.json(dt.data);
    })
  },

  resubscribeCustomer: function(req, res) {
    var  postData = {
      plan_code: req.param("plan_id"),
      account: {
        account_code: req.param("id")
      },
      currency: "USD"
    };
    console.log(postData);
    recurly.subscriptions.create(postData, function(error, dt){
      console.log('this came back with');
      console.log('error: ' + error);
      console.log('data: ' + dt);
      if(error) {
        if(error.statusCode === 503){
          return res.status(503).send({
            message: 'Recurly servers down'
          });
        }
        return res.negotiate(error);
      }

      if(dt.data && dt.data.subscription && dt.data.subscription.invoice &&
        dt.data.subscription.invoice.$ && dt.data.subscription.invoice.$.href){
        var urlInvoice = dt.data.subscription.invoice.$.href;
        var invoiceNumber = urlInvoice.substring(urlInvoice.lastIndexOf('/') + 1);
        dt.data.invoiceNum = invoiceNumber;
      }

      return res.json(dt.data);
    })
  },

  updateSubscription: function(req, res) {
	  var data = {
	    'subscription_id': req.param('subscriptionId'),
      'plan_id': req.param('planId')
    };

	  this.post('/update/subscription', data, function(data) {
	    return res.json(data);
    });
  },

  unsubscribe: function(req, res) {
	  var data = {
	    'subscription_id': req.param('subscriptionId')
    };

	  this.post('unsubscribe/customer', data, function (data) {
      return res.json(data);
    });

  },

  get: function(endpoint, params, callback) {
    var headers = {'api_key': this.api_key, 'api_secret': this.api_secret};

    var options = {
      url: endpoint,
      baseUrl: this.api_base,
      headers: headers,
      qs: params
    };
    console.log('endpoint: ' + endpoint);
    console.log('params: ' + JSON.stringify(params));


    request(options, function (error, response, body) {
      if (!error && response.statusCode == 200) {
        var info = JSON.parse(body);
        return callback(info);
      } else {
        console.log('Request Error (POST): ' + error);
        console.log('Request Error (RESPONSE): ' + response);
        console.log('Request Error (BODY): ' + body);
        var infoError = {
          message: JSON.parse(body).error
        };
        return callback(undefined,infoError);
      }
    });
  },

  post: function(endpoint, params, callback) {
    var headers = {'api_key': this.api_key, 'api_secret': this.api_secret};

    var options = {
      url: endpoint,
      baseUrl: this.api_base,
      headers: headers,
      method: 'POST',
      form: params
    };
    console.log('endpoint: ' + endpoint);
    console.log('params: ' + JSON.stringify(params));

    request(options, function(error, response, body) {
      if (!error && response.statusCode == 200) {
        var info = JSON.parse(body);
        return callback(info);
      } else {
        console.log('Request Error (POST): ' + error);
        console.log('Request Error (RESPONSE): ' + response);
        console.log('Request Error (BODY): ' + body);
        var infoError = {
          message: JSON.parse(body).error
        };
        return callback(undefined,infoError);
      }
    });
  }


};

function getInvoiceListPrivate(paidIdsArray ,id){
  var promiseArray = [];
  var invoiceList = [];
  paidIdsArray.push(parseInt(id));

  if(paidIdsArray.length > 0){
    console.log(paidIdsArray);
    paidIdsArray.forEach(function(id){
      var recurlyProm = Promise.promisify(recurly.invoices.listByAccount);
      promiseArray.push(recurlyProm(id, {state: 'collected'}).then(function(dt){
        if(dt.data.invoices.invoice instanceof Array){
          dt.data.invoices.invoice.forEach(function(invoice){
            invoiceList.push({
              invoice_number: invoice.invoice_number._,
              closed_date: invoice.closed_at._,
              amt: invoice.total_in_cents._ / 100
            })
          });
        } else {
          if(dt.data && dt.data.invoices && dt.data.invoices.invoice){
            var item = dt.data.invoices.invoice;
            invoiceList.push({
              invoice_number: item.invoice_number._,
              closed_date: item.closed_at._,
              amt: item.total_in_cents._ / 100
            })
          }
        }

        return new Promise(function(resolve) {
          resolve('ya');
        });
      }));

    })
  }
  return Promise.all(promiseArray).then(function(){
    console.log(invoiceList);
    console.log('ASDASD');
    return new Promise(function(resolve) {
      resolve(invoiceList);
    });
  }).error(function (e) {
    console.log('blah')
    console.log(e);
    return new Promise(function(resolve, reject) {
      reject(e);
    });
  });
}

function randomString(length) {
  return Math.round((Math.pow(36, length + 1) - Math.random() * Math.pow(36, length))).toString(36).slice(1);
}

function validatePass(str) {
  var uppercase = 0;
  var lowercase = 0;
  var num = 0;
  var special = 0;
  var characterList = "!#'$%&()*+,-.:;<=>?@[]^_`{|}\"\\";

  for(i=0;i<str.length;i++)
  {
    if('A' <= str[i] && str[i] <= 'Z') // check if you have an uppercase
      uppercase++;
    if('a' <= str[i] && str[i] <= 'z') // check if you have a lowercase
      lowercase++;
    if('0' <= str[i] && str[i] <= '9') // check if you have a numeric
      num++;
    if(characterList.indexOf(str[i]) > -1){
      special++;
    }
  }
  return !(uppercase === 0 || lowercase === 0 || num === 0 || special === 0);
}
