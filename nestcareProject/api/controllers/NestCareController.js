/**
 * PayController
 *
 * @description :: Server-side logic for managing Nestcare Calls Mirros nestcare API.
 * @help        :: See http://sailsjs.org/#!/documentation/concepts/Controllers
 */

// -- Data flows in from outside ->
// -- Requestjs sends request out
// -- Data comes back, Requestjs calls its callback.
// -- Requestjs callback calls OUR callback
// -- Our callback forwards data back via sails, request complete.
// -- Add more routes as they become needed.
var request = require('request');
var payCtrl = require('./PayController');
var jwt = require('jwt-simple');
var pgQuery = require('../services/customSQL');

var configEnv = require('../services/configEnv')();
var Recurly = require('node-recurly');
var discourse_sso = require('discourse-sso');
var sso = new discourse_sso("0f10ceaf-8310-47ea-a94f-80709988ab89");
var GoogleURL = require( 'google-url' );
var Promise = require("bluebird");
var recurly = new Recurly(configEnv);
module.exports = {
  api_base: 'http://54.82.77.38:8001/api',
  api_key: '',
  api_secret: '',
  auth: '',
  stateHash: {
    "AL": "1",
    "AK": "2",
    "AZ": "3",
    "AR": "4",
    "CA": "5",
    "CO": "6",
    "CT": "7",
    "DE": "8",
    "FL": "9",
    "GA": "10",
    "HI": "11",
    "ID": "12",
    "IL": "13",
    "IN": "14",
    "IA": "15",
    "KS": "16",
    "KY": "17",
    "LA": "18",
    "ME": "19",
    "MD": "20",
    "MA": "21",
    "MI": "22",
    "MN": "23",
    "MS": "24",
    "MO": "25",
    "MT": "26",
    "NE": "27",
    "NV": "28",
    "NH": "29",
    "NJ": "30",
    "NM": "31",
    "NY": "32",
    "NC": "33",
    "ND": "34",
    "OH": "35",
    "OK": "36",
    "OR": "37",
    "PA": "38",
    "RI": "39",
    "SC": "40",
    "SD": "41",
    "TN": "42",
    "TX": "43",
    "UT": "44",
    "VT": "45",
    "VA": "46",
    "WA": "47",
    "WV": "48",
    "WI": "49",
    "WY": "50"
  },
    stateHashReverse: {
      "1": "AL",
      "2": "AK",
      "3": "AZ",
      "4": "AR",
      "5": "CA",
      "6": "CO",
      "7": "CT",
      "8": "DE",
      "9": "FL",
      "10": "GA",
      "11": "HI",
      "12": "ID",
      "13": "IL",
      "14": "IN",
      "15": "IA",
      "16": "KS",
      "17": "KY",
      "18": "LA",
      "19": "ME",
      "20": "MD",
      "21": "MA",
      "22": "MI",
      "23": "MN",
      "24": "MS",
      "25": "MO",
      "26": "MT",
      "27": "NE",
      "28": "NV",
      "29": "NH",
      "30": "NJ",
      "31": "NM",
      "32": "NY",
      "33": "NC",
      "34": "ND",
      "35": "OH",
      "36": "OK",
      "37": "OR",
      "38": "PA",
      "39": "RI",
      "40": "SC",
      "41": "SD",
      "42": "TN",
      "43": "TX",
      "44": "UT",
      "45": "VT",
      "46": "VA",
      "47": "WA",
      "48": "WV",
      "49": "WI",
      "50": "WY"
    },

  resetPassword: function(req, res) {
    this.post('/auth/forgotPassword', req.allParams(), function(data, error) {
      if (error) {
        return res.json(error.message);
      }

      return res.json(data);
    });
  },

  ssoLogout: function(req, res) {

  },

  doSso: function(req, res) {
    var params = req.allParams();
    console.log(params);
    var q = sso.buildLoginString(req.allParams());
    console.log(q);

    return res.json(q);
  },

  ssoValidate: function(req, res) {
    var params = req.allParams();
    var payload = params['payload'];
    var sig = params['sig'];
    return res.json(sso.validate(payload, sig));
  },

  getNonce: function(req, res) {
    var params = req.allParams();
    var nonce = params['nonce'];
    console.log('I was called!');
    console.log('incoming: ' + nonce);
    var out = sso.getNonce(nonce);
    console.log('out: ' + out);
    return res.json(out);
  },

  register: function(req, res) {
    this.post('/auth/register', req.allParams(), function(dt, error) {
      console.log('this came back with');
      console.log('error: ' + error);
      console.log('data: ' + JSON.stringify(dt));

      if(error){
        console.log('HHHAAA');
        console.log(error);
        console.log(error.message.message);
        if(error.message && error.message.message.indexOf('should be 8 characters or longer and contain') > -1){

          return res.status(422).send({
            message: {
              message: 'Password should be 8 characters or longer and contain at least one uppercase letter, one lowercase character, one number, and one special character(e.g. $,$,%,^'
            }
          })
        }
        return res.status(422).send({
          message: {
            message: 'Registration Error Occured. Please Contact Support.'
          }
        })
      }

      var payload = {
        sub: dt.data.user_id
      };
      dt.token = jwt.encode(payload, configEnv.sailssecret);

      return res.json(dt);
    });
  },

  twofactorVerify: function(req, res) {
    var ctrl = this;

    var postData = {
      auth_code: req.param("auth_code"),
      user_id: req.param("id")
    };
    this.post('/auth/2fa', postData, function(loginResp, error) {
      console.log('this came back with');
      console.log('error: ' + error);

      if(error){
        return res.status(422).send({
          message: {
            message: 'Registration Error Occured. Please Contact Support.'
          }
        })
      }

      if(loginResp && loginResp.data && loginResp.data.address && loginResp.data.address.home && loginResp.data.address.home.state_id){
        loginResp.data.address.home.state_id = ctrl.stateHashReverse[loginResp.data.address.home.state_id + ''];
      }

      console.log('GOING TO HIT RECURLY');
      var recurlyId = loginResp.data.id;
      recurly.accounts.get(recurlyId, function(error, dt){
        console.log('this came back with');
        console.log('error: ' + error);
        console.log('data: ' + dt);
        if(error) {
          console.log(error);
          if(error.statusCode !== 404){
            return res.json(loginResp);
          }
        }
        if(dt){
          loginResp.data.paywhirl = dt.data;
          var payload = {
            sub: dt.data.account.account_code
          };
          loginResp.token = jwt.encode(payload, configEnv.sailssecret);
        }

        return res.json(loginResp);
      });
    });
  },


  getCustomer: function(req, res) {
    var ctrl = this;


    if(req.headers.authorization){
      this.auth = req.headers.authorization;
    }


    this.get('/user', {}, function(dt, error) {
      if(error){
        if(error.message && error.message.status_code === 401 && error.message.message === 'Token has expired'){
          return res.status(422).send({
            message: {
              message: 'Token has expired'
            }
          });
        } else {
          return res.status(422).send({
            message: {
              message: 'Could not retrieve Customer'
            }
          })
        }
      }
      if(dt){
        var payload = {
          sub: dt.data.id
        };
        dt.data.token = jwt.encode(payload, configEnv.sailssecret);
      }
      if(dt && dt.data && dt.data.address && dt.data.address.home && dt.data.address.home.state_id){
        dt.data.address.home.state_id = ctrl.stateHashReverse[dt.data.address.home.state_id + ''];
      }

      return res.json(dt.data);
    });
  },

  adminGetCustomer: function(req, res) {
    var ctrl = this;


    if(req.headers.authorization){
      this.auth = req.headers.authorization;
    }

    this.get('/admin/user/' + req.param('id'), {}, function(dt, error) {
      if(error){
        return res.negotiate(error);
      }
      if(dt && dt.data && dt.data.address && dt.data.address && dt.data.address.state_id){
        dt.data.address.state_id = ctrl.stateHashReverse[dt.data.address.state_id + ''];
        dt.data.address.city_id = dt.data.address.city_name;
      };
      return res.json(dt.data);
    });
  },

  adminGetAllNestcareCustomers: function(req, res) {
    var ctrl = this;


    if(req.headers.authorization){
      this.auth = req.headers.authorization;
    }

    this.get('/admin/users/1', {}, function(dt, error) {
      if(error){
        return res.negotiate(error);
      }
      return res.json(dt.data);
    });
  },

  adminUpdateAccountVerified: function(req, res) {
    var postData = {
      id: req.param('id'),
      status_id: req.param('status_id')
    };

    if(req.headers.authorization){
      this.auth = req.headers.authorization;
    }

    this.post('/admin/user/change_status', postData, function(dt, error) {
      if(error){
        return res.negotiate(error);
      }
      return res.json(dt);
    });
  },

  adminUpdateUserDetails: function(req, res) {
    var postData = {
      first_name: req.param('first_name'),
      last_name: req.param('last_name'),
      email: req.param('email'),
      dob: req.param('dob'),
      gender: req.param('gender'),
      mobile: req.param('mobile'),
      id: req.param('id')
    };

    if(req.headers.authorization){
      this.auth = req.headers.authorization;
    }

    this.post('/admin/user', postData, function(dt, error) {
      if(error){
        return res.negotiate(error);
      }
      return res.json(dt.data);
    });
  },

  adminLogin: function(req, res) {
    var ctrl = this;
    this.post('/admin/login', req.allParams(), function(loginResp, error) {
      if(error){
        console.log(error);
        if(error.message && error.message.message === '404 Not Found'){
          return res.status(422).send({
            message: 'We could not connect to the admin server.  Please try again later.'
          })
        }
        return res.status(422).send({
          message: error.message.message
        })
      }
      console.log(loginResp);
      var payload = {
        sub: {},
        isAdmin: true
      }
      loginResp.token = jwt.encode(payload, configEnv.sailssecret);

      return res.json(loginResp);
    });
  },

  login: function(req, res) {
    var ctrl = this;
    this.post('/auth/login', req.allParams(), function(loginResp, errorOne) {
      if(errorOne){
        return res.status(422).send({
          message: errorOne.message.message
        })
      }
      console.log(loginResp);
      if(loginResp && loginResp.data && loginResp.data.address && loginResp.data.address.home && loginResp.data.address.home.state_id){
        loginResp.data.address.home.state_id = ctrl.stateHashReverse[loginResp.data.address.home.state_id + ''];
      }

      console.log('GOING TO HIT RECURLY');
      var recurlyId = loginResp.data.id;
      recurly.accounts.get(recurlyId, function(error, dt){
        console.log('this came back with');
        console.log('error: ' + error);
        console.log('data: ' + dt);
        if(error) {
          console.log(error);
          var payload = {
            sub: ''
          };
          loginResp.token = jwt.encode(payload, configEnv.sailssecret);
          return res.json(loginResp);
        }
        if(dt){
          loginResp.data.paywhirl = dt.data;
          var payload = {
            sub: dt.data.account.account_code
          };
          loginResp.token = jwt.encode(payload, configEnv.sailssecret);
        }

        return res.json(loginResp);
      });

    });
  },

  viewCompletedOrdersHistory: function(req, res) {

    var fromDate = '';
    var todayDate = new Date();
    var toDate = req.param('toDate');
    var fromDate = req.param('fromDate');
    console.log(fromDate);
    if(!toDate){
      toDate = todayDate.getFullYear() + '-' + ('0' + (todayDate.getMonth()+1)).slice(-2) + '-' + ('0' + (todayDate.getDate())).slice(-2)
    }

    if(!fromDate){
      var lastMonth = new Date(new Date().setDate(todayDate.getDate()-30));
      console.log(lastMonth);
      fromDate = lastMonth.getFullYear() + '-' + ('0' + (lastMonth.getMonth()+1)).slice(-2) + '-' + ('0' + (lastMonth.getDate())).slice(-2)
    }

    console.log(fromDate);

    pgQuery.getCompletedOrdersHistory(fromDate, toDate,function(dt, error){
      if(error){
        return res.status(422).send({
          message: {
            message: 'Could not retrieve Completed Orders History'
          }
        })
      }
      if(dt){
        dt.forEach(function(item){
          if(item.shippo_id === 'null'){
            item.shippo_id = '';
          }

          if(item.shipservice === 'null'){
            item.shipservice = '';
          }

          var splitItems = item.shippo_id.split(',');

          item.deviceOrder = splitItems[0];
          item.invoiceNumber = splitItems[1];
          item.shipamt = splitItems[3];
          item.shippingAddress = splitItems[2];

        })

        return res.json(dt);
      } else {
        return res.status(422).send({
          message: {
            message: 'Could not retrieve Completed Orders History'
          }
        })
      }

    });

  },

  checkIfAuthorized: function(req, res) {


    pgQuery.getUsersPaidFor(req.param('id'),function(data, error){
      if(error){
        return res.status(422).send({
          message: {
            message: 'Could not retrieve Paid Users'
          }
        })
      }

      listIds = data.map(function(item){
        return item.paid_user_id;
      });

      var payload = {
        sub: req.param('id'),
        usersAuthorized: listIds
      };
      var dt = {
        message: '',
        token: undefined
      };
      dt.message = "success";
      dt.token = jwt.encode(payload, configEnv.sailssecret);

      console.log(payload);
      return res.json(dt);
    });

  },

//
  updateProfile: function(req, res) {
    this.post('/profile', req.allParams(), function(data, error) {
      if(error){
        return res.negotiate(error);
      }
      return res.json(data);
    });
  },

  getWellnessActivities: function(req, res) {

    if(req.headers.authorization){
      this.auth = req.headers.authorization;
    }

    this.get('/wellness/measurements/' + req.param('date'), {}, function(data, error) {
      if(error){
        return res.negotiate(error);
      }
      return res.json(data);
    });
  },

  getWellnessSchedules: function(req, res) {

    if(req.headers.authorization){
      this.auth = req.headers.authorization;
    }

    this.get('/wellness', {}, function(data, error) {
      if(error){
        return res.negotiate(error);
      }
      return res.json(data);
    });
  },

  medicationSchedules: function(req, res) {
    if(req.headers.authorization){
      this.auth = req.headers.authorization;
    }

    this.post('/medications', {}, function(data, error) {
      if(error){
        return res.negotiate(error);
      }
      return res.json(data);
    });
  },

  getMedicationActivities: function(req, res) {
    if(req.headers.authorization){
      this.auth = req.headers.authorization;
    }

    this.get('/medication/measurements/' + req.param('date'), {}, function(data, error) {
      if(error){
        return res.negotiate(error);
      }
      return res.json(data);
    });
  },

  getVitalMeasurements: function(req, res) {
    this.get('/vital/measurements/' + req.param('date'), {}, function(data, error) {
      if(error){
        return res.negotiate(error);
      }
      return res.json(data);
    });
  },

  getVitalSchedules: function(req, res) {
    console.log('this is it');
    this.get('/vital', {}, function(data, error) {
      console.log(data);
      console.log(error);
      console.log('got this');
      if(error){
        return res.negotiate(error);
      }
      return res.json(data);
    });
  },

  updateShipping: function(req, res) {
    if(req.headers.authorization){
      this.auth = req.headers.authorization;
    }
    console.log('YA');
    console.log(req.param('state'));
    console.log(this.stateHash[req.param('state')]);
    var postData = {
      home_line1: req.param('address_line1'),
      home_line2: req.param('address_line2'),
      home_city_id: req.param('city'),
      home_state_id: this.stateHash[req.param('state')],
      home_zip_code: req.param('zip_code'),
      home_country_id: 1,
      home_phone: req.param('phone')
    };

    this.post('/shipping', postData, function(data, error) {
      if(error){
        return res.negotiate(error);
      }
      return res.json(data);
    });
  },

  updateShippingAdmin: function(req, res) {
    if(req.headers.authorization){
      this.auth = req.headers.authorization;
    }
    console.log('YA');
    console.log(req.param('state'));
    console.log(this.stateHash[req.param('state')]);
    var postData = {
      line1: req.param('address_line1'),
      line2: req.param('address_line2'),
      city_name: req.param('city'),
      state_id: this.stateHash[req.param('state')],
      zip_code: req.param('zip_code'),
      country_id: 1,
      billing: req.param('billing'),
      phone: req.param('phone'),
      user_id: req.param('id')
    };


    this.post('/admin/address', postData, function(data, error) {
      if(error){
        return res.negotiate(error);
      }
      return res.json(data);
    });
  },

  updateShippingAdminAsAuthorizedUser: function(req, res) {
    if(req.headers.authorization){
      this.auth = configEnv.adminNestcareHash;
    }

    var postData = {
      line1: req.param('address_line1'),
      line2: req.param('address_line2'),
      city_name: req.param('city'),
      state_id: this.stateHash[req.param('state')],
      zip_code: req.param('zip_code'),
      country_id: 1,
      billing: req.param('billing'),
      phone: req.param('phone'),
      user_id: req.param('id')
    };

    console.log('asdasd');
    console.log(postData);

    this.post('/admin/address', postData, function(data, error) {
      if(error){
        return res.negotiate(error);
      }
      return res.json(data);
    });
  },


  updateBilling: function(req, res) {
    if(req.headers.authorization){
      this.auth = req.headers.authorization;
    }
    console.log('AUTHORIZATION UPDATE BILLING================');
    console.log(this.auth);
    var postData = {
      bill_line1: req.param('address_line1'),
      bill_line2: req.param('address_line2'),
      bill_city_id: req.param('city'),
      bill_state_id: this.stateHash[req.param('state')],
      bill_zip_code: req.param('zip_code'),
      bill_country_id: 1,
      bill_phone: req.param('phone')
    };

    this.post('/billing', postData, function(data, error) {
      if(error){
        return res.negotiate(error);
      }
      return res.json(data);
    });
  },
  getUsersPaidFor: function(req, res) {

    pgQuery.getUsersPaidFor(req.param('paying_user'),function(data, error){
      if(error){
        return res.status(422).send({
          message: {
            message: 'Could not retrieve Paid Users'
          }
        })
      }

      return res.json(data);
    });

  },

  getPayingUser: function(req, res) {

    pgQuery.getPayingUser(req.param('paid_user'),function(data, error){
      if(error){
        return res.status(422).send({
          message: {
            message: 'Could not retrieve Paid User'
          }
        })
      }

      return res.json(data);
    });

  },

  removePaidUser: function(req, res) {

    pgQuery.deletePaidFor(req.param('paid_user'),function(data, error){
      if(error){
        return res.status(422).send({
          message: {
            message: 'Could not remove Paid User'
          }
        })
      }

      return res.json(data);
    });

  },

  insertPaidUser: function(req, res) {

    pgQuery.insertPaidFor(req.param('id'), req.param('paid_user'),function(data, error){
      if(error){
        return res.status(422).send({
          message: {
            message: 'Could not add pay user'
          }
        })
      }

      return res.json(data);
    });

  },

  addPaymentCustomerNumber: function(req, res) {
    if(req.headers.authorization){
      this.auth = req.headers.authorization;
    }

    var postData = {
      payment_customer_number: req.param('payment_customer_number'),
      user_id: req.param('id')
    };
    this.post('/add_payment_detail', postData, function(data, error) {
      console.log('this came back with');
      console.log('error: ' + error);
      console.log('data: ' + JSON.stringify(data));

      if(error){
        return res.status(422).send({
          message: {
            message: 'Registration Error Occured. Please Contact Support.'
          }
        })
      }

      return res.json(data);
    });
  },

  getPeopleYouSupport: function(req, res) {
    if(req.headers.authorization){
      this.auth = req.headers.authorization;
    }

    this.get('/support/people', {}, function(dt, error) {
      console.log('this came back with');
      console.log('error: ' + error);
      console.log('data: ' + JSON.stringify(dt));

      if(error){
        return res.status(422).send({
          message: {
            message: 'Could not retrieve people you support. Contact Support'
          }
        })
      }

      return res.json(dt.data);
    });
  },

  addCompletedOrderTracker: function(req, res) {
    if(req.headers.authorization){
      this.auth = req.headers.authorization;
    }

    var postData = {
      payment_customer_number: req.param('paying_cust'),
      shippo_id: (req.param('shippo_id')) ? req.param('shippo_id') : 'null',
      tracking_number: (req.param('tracking_number')) ? req.param('tracking_number') : 'null',
      status: req.param('status')
    };
    console.log(postData);

    this.post('/add_payment', postData, function(data, error) {
      console.log('this came back with');
      console.log('error: ' + error);
      console.log('data: ' + JSON.stringify(data));

      if(error){
        return res.status(422).send({
          message: {
            message: 'Registration Error Occured. Please Contact Support.'
          }
        })
      }

      return res.json(data);
    });
  },

  changeAccountType: function(req, res) {
    if(req.headers.authorization){
      this.auth = req.headers.authorization;
    }

    var postData = {
      old_account_type_id: req.param('old_account_type_id'),
      new_account_type_id: req.param('new_account_type_id')
    };

    this.post('/change_account_type', postData, function(data, error) {
      console.log('this came back with');
      console.log('error: ' + error);
      console.log('data: ' + JSON.stringify(data));

      if(error){
        return res.status(422).send({
          message: {
            message: 'Account Change Error Occured. Please Contact Support.'
          }
        })
      }

      return res.json(data);
    });
  },

  changeAccountTypeAdmin: function(req, res) {

    var postData = {
      user_id: req.param('user_id'),
      new_account_type_id: req.param('new_account_type_id')
    };
    console.log('this came back with');
    console.log('error: ' + error);
    console.log('data: ' + JSON.stringify(data));
    pgQuery.changeAccountType(postData.user_id, postData.new_account_type_id,function(data, error){
      if(error){
        return res.status(422).send({
          message: {
            message: 'Could not change account type for User'
          }
        })
      }

      return res.json(data);
    });

  },

//
//   updateCustomer: function(req, res) {
//     this.post('/update/customer', req.allParams(), function(data) {
//       return res.json(data);
//     });
//   },
//
//   updateAnswer: function(req, res) {
//     this.post('/update/answer', req.allParams(), function(data) {
//       return res.json(data);
//     });
//   },
//
//   getQuestions: function(req, res) {
//     this.get('/questions', req.allParams(), function(data) {
//       return res.json(data);
//     });
//   },
//
//   getPlans: function(req, res) {
//     this.get('/plans', req.allParams(), function(data) {
//       return res.json(data);
//     });
//   },
//
//   getPlan: function(req, res) {
//     this.get('/plan/' + req.param('id'), [], function(data) {
//       return res.json(data);
//     });
//   },
//
//   createPlan: function(req, res) {
//     this.post('/create/plan', req.allParams(), function(data) {
//       return res.json(data);
//     });
//   },
//
//   updatePlan: function(req, res) {
//     this.post('/update/plan', req.allParams(), function(data) {
//       return res.json(data);
//     });
//   },
//
//   getSubscriptions: function(req, res) {
//     this.get('/subscriptions/' + req.param('id'), [], function(data) {
//       return res.json(data);
//     });
//   },
// // -- TODO: GOSHIPPO API!
//   getSubscription: function(req, res) {
//     this.get('/subscription/' + req.param('id'), [], function(data) {
//       return res.json(data);
//     });
//   },
//
//   subscribeCustomer: function(req, res) {
//     var data = {
//       'customer_id': req.param('customerId'),
//       'plan_id': req.param('planId')
//     };
//
//     if (req.param('trialEnd')) {
//       data['trial_end'] = req.param('trialEnd');
//     }
//
//     this.post('/subscribe/customer', data, function(data) {
//       return res.json(data);
//     });
//   },
//
//   updateSubscription: function(req, res) {
//     var data = {
//       'subscription_id': req.param('subscriptionId'),
//       'plan_id': req.param('planId')
//     };
//
//     this.post('/update/subscription', data, function(data) {
//       return res.json(data);
//     });
//   },
//
//   unsubscribe: function(req, res) {
//     var data = {
//       'subscription_id': req.param('subscriptionId')
//     };
//
//     this.post('unsubscribe/customer', data, function (data) {
//       return res.json(data);
//     });
//
//   },

  get: function(endpoint, params, callback) {
    var headers = {'api_key': this.api_key, 'api_secret': this.api_secret,
      'Authorization': this.auth};

    var options = {
      url: endpoint,
      baseUrl: this.api_base,
      headers: headers,
      qs: params
    };

    request(options, function (error, response, body) {
      if (!error && response.statusCode == 200) {
        var info = JSON.parse(body);
        return callback(info);
      } else {
        var infoError = JSON.parse(body);
        console.log('Request Error (POST): ' + error);
        console.log('Request Error (RESPONSE): ' + response);
        console.log('Request Error (BODY): ' + body);
        var infoError = {
          message: JSON.parse(body)
        };
        return callback(undefined,infoError);
      }
    });
  },

  post: function(endpoint, params, callback) {
    var headers = {'api_key': this.api_key, 'api_secret': this.api_secret,
      'Authorization': this.auth};

    var options = {
      url: endpoint,
      baseUrl: this.api_base,
      headers: headers,
      method: 'POST',
      form: params
    };
    console.log('endpoint: ' + endpoint);
    console.log('params: ' + JSON.stringify(params));

    request(options, function(error, response, body) {
      if (!error && response.statusCode == 200) {
        var info = JSON.parse(body);
        return callback(info);
      } else {
        var infoError = JSON.parse(body);
        console.log('Request Error (POST): ' + error);
        console.log('Request Error (RESPONSE): ' + response);
        console.log('Request Error (BODY): ' + body);
        var infoError = {
          message: JSON.parse(body)
        };
        return callback(undefined,infoError);
      }
    });
  }
};

