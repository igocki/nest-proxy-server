/**
 * ShippoController
 *
 * @description :: Server-side logic for managing the GoShippo API.
 * @help        :: See http://sailsjs.org/#!/documentation/concepts/Controllers
 */
var configEnv = require('../services/configEnv')();
var shippo = require('shippo')(configEnv.shippoAPIKEY);
var request = require ('request');

module.exports = {
  private_key: configEnv.shippoAPIKEY,
  api_base: 'https://api.goshippo.com/',

  verifyAddress: function(req, res) {
    var val = req.allParams();
    val.validate = true;

    shippo.address.create(val, function(err, address) {
      if(err){
        return res.negotiate(err);
      }
      console.log('asdasd');
      console.log(address);
      if(address && address.object_state === 'INVALID'){
        return res.status(422).send({
          message: "This address came back invalid, please try a valid address."
        })
      }

      return res.json(address);
    });
  },

  createShipment: function(req, res) {
    console.log(JSON.stringify(req.allParams()));
    shippo.shipment.create(req.allParams(), function(err,shipment) {
      if(err){
        return res.negotiate(err);
      }
      return res.json(shipment);
    });
  },

  createRefund: function(req, res) {
    shippo.refund.create(req.allParams(), function(err, refund) {
      if(err){
        return res.negotiate(err);
      }

      if(refund && refund.object_status === 'ERROR'){
        return res.status(422).send({
          message: "Your order has been fulfilled however there is a problem with the shipment information.  please contact customer support."
        })
      }

      return res.json(refund);
    });
  },

  createLabel: function(req, res) {
    shippo.transaction.create(req.allParams(), function(err, transaction) {
      if(err){
        return res.negotiate(err);
      }
      console.log(transaction);
      if(transaction && transaction.object_status === 'ERROR'){
        return res.status(422).send({
          message: "Your order has been fulfilled however there is a problem with the shipment information.  please contact customer support."
        })
      }

      return res.json(transaction);
    });
  },

  trackPackage: function(req, res) {
    this.get('tracks/' + req.param('carrier') + '/' + req.param('tracking'), {}, function(data) {
      return res.json(data);
    })
  },

  get: function(endpoint, params, callback) {
    var headers = {'Authorization': 'ShippoToken ' + this.private_key};

    var options = {
      url: endpoint,
      baseUrl: this.api_base,
      headers: headers,
      qs: params
    };

    console.log('endpoint: ' + endpoint);
    console.log('params: ' + JSON.stringify(params));

    request(options, function (error, response, body) {
      if (!error && response.statusCode == 200) {
        var info = JSON.parse(body);
        return callback(info);
      } else {
        console.log('Request Error: ' + error);
        var infoError = JSON.parse(body);
        return callback(infoError);
      }
    });
  },
};

