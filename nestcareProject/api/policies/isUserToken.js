var jwt = require('jwt-simple');
var configEnv = require('../services/configEnv')();
module.exports = function(req, res, next){

  if(!req.headers || !req.headers.authorizationig){
    return res.forbidden();
  }
  var token = req.headers.authorizationig.split(' ')[1];
  console.log('IN USER AUTHORIZATION');
  var payload = jwt.decode(token, configEnv.sailssecret);
  if((!payload.sub || (payload.sub + '') !== req.params.all().id + '') && !payload.isAdmin){
    console.log('FAILED');
    console.log(payload);
    console.log(req.params.all());
    return res.forbidden();
  }

  next();
};
