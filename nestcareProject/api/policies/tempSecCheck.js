var jwt = require('jwt-simple');
var configEnv = require('../services/configEnv')();
module.exports = function(req, res, next){

  if(!req.headers || !req.headers.authorizationig){
    return res.forbidden();
  }
  var token = req.headers.authorizationig.split(' ')[1];
  console.log('IN Temp AUTH');
  console.log(token);
  var payload = jwt.decode(token, configEnv.sailssecretTemp);
  console.log(payload);
  if(!payload.sub && !payload.isAdmin){
    console.log('FORBID Temp AUTH');
    return res.forbidden();
  }

  next();
};
