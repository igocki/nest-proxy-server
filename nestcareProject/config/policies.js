/**
 * Policy Mappings
 * (sails.config.policies)
 *
 * Policies are simple functions which run **before** your controllers.
 * You can apply one or more policies to a given controller, or protect
 * its actions individually.
 *
 * Any policy file (e.g. `api/policies/authenticated.js`) can be accessed
 * below by its filename, minus the extension, (e.g. "authenticated")
 *
 * For more information on how policies work, see:
 * http://sailsjs.org/#!/documentation/concepts/Policies
 *
 * For more information on configuring policies, check out:
 * http://sailsjs.org/#!/documentation/reference/sails.config/sails.config.policies.html
 */


module.exports.policies = {

  'PayController':{
    'updateCustomer': ['jwtAuth', 'isUserToken'],
    'createCard': ['jwtAuth', 'isUsersAuthorized'],
    'deleteCard': ['jwtAuth', 'isUsersAuthorized'],
    'subscribeNewCustomer': ['jwtAuth', 'isUserToken'],
    'resubscribeCustomer': ['jwtAuth', 'isUserToken'],
    'createCharge': ['jwtAuth', 'isUserToken'],
    'getCards': ['jwtAuth', 'isUsersAuthorized'],
    'getInvoiceSecCheck': ['jwtAuth', 'isUserToken'],
    'getCustomer': ['jwtAuth'],
    'getCollectedInvoices': ['jwtAuth', 'isUserToken']
    // 'findOne': ['jwtAuth'],
    // 'logout': ['jwtAuth'],
    // 'destroy': ['jwtAuth', 'isUserAdmin'],
    // 'update': ['jwtAuth', 'isUserToken'],
    // 'updateInterests': ['jwtAuth', 'isUserToken'],
    // 'updateOtherInterests': ['jwtAuth', 'isUserToken'],
    // 'addPaymentCard':  ['jwtAuth', 'isUserToken'],
    // 'getPaymentCards':  ['jwtAuth', 'isUserToken'],
    // 'deletePaymentCard':  ['jwtAuth', 'isUserToken'],
    // 'updateDefaultCard':  ['jwtAuth', 'isUserToken'],
    // 'completeBooking':  ['jwtAuth', 'isUserToken']
  },
  'NestCareController':{
    'addPaymentCustomerNumber': ['jwtAuth', 'isUserToken'],
    'updateShippingAdmin': ['isAdmin'],
    'changeAccountTypeAdmin': ['isAdmin'],
    'insertPaidUser': ['isPaymentSourceId'],
    'checkIfAuthorized': ['jwtAuth', 'isUserToken'],
    'updateShippingAdminAsAuthorizedUser': ['jwtAuth', 'isUsersAuthorized'],
    'adminUpdateUserDetails': ['isAdmin']
    // 'findOne': ['jwtAuth'],
    // 'logout': ['jwtAuth'],
    // 'destroy': ['jwtAuth', 'isUserAdmin'],
    // 'update': ['jwtAuth', 'isUserToken'],
    // 'updateInterests': ['jwtAuth', 'isUserToken'],
    // 'updateOtherInterests': ['jwtAuth', 'isUserToken'],
    // 'addPaymentCard':  ['jwtAuth', 'isUserToken'],
    // 'getPaymentCards':  ['jwtAuth', 'isUserToken'],
    // 'deletePaymentCard':  ['jwtAuth', 'isUserToken'],
    // 'updateDefaultCard':  ['jwtAuth', 'isUserToken'],
    // 'completeBooking':  ['jwtAuth', 'isUserToken']
  }

};
