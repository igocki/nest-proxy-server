/**
 * Route Mappings
 * (sails.config.routes)
 *
 * Your routes map URLs to views and controllers.
 *
 * If Sails receives a URL that doesn't match any of the routes below,
 * it will check for matching files (images, scripts, stylesheets, etc.)
 * in your assets directory.  e.g. `http://localhost:1337/images/foo.jpg`
 * might match an image file: `/assets/images/foo.jpg`
 *
 * Finally, if those don't match either, the default 404 handler is triggered.
 * See `api/responses/notFound.js` to adjust your app's 404 logic.
 *
 * Note: Sails doesn't ACTUALLY serve stuff from `assets`-- the default Gruntfile in Sails copies
 * flat files from `assets` to `.tmp/public`.  This allows you to do things like compile LESS or
 * CoffeeScript for the front-end.
 *
 * For more information on configuring custom routes, check out:
 * http://sailsjs.org/#!/documentation/concepts/Routes/RouteTargetSyntax.html
 */

module.exports.routes = {

  'get /crossorigin': 'CrossOriginController.getCrossOrigin',
  'post /crossorigin': 'CrossOriginController.postCrossOrigin',

  // -- Paywhirl Controller
  'get /pay/active_customers': 'PayController.getActiveCustomers',
  'get /pay/all_subscribed_customers': 'PayController.getAllSubscribedCustomers',
  'get /pay/customer': 'PayController.getCustomer',
  'get /pay/get_active_subscriptions': 'PayController.getActiveSubscriptions',
  'get /pay/get_collected_invoices': 'PayController.getCollectedInvoices',
  'get /pay/getInvoicePdf': 'PayController.getInvoicePdf',
  'get /pay/getInvoiceSecCheck': 'PayController.getInvoiceSecCheck',
  'post /pay/customer': 'PayController.createCustomer',
  'put /pay/customer': 'PayController.updateCustomer',
  'put /pay/answer': 'PayController.updateAnswer',
  'get /pay/questions': 'PayController.getQuestions',
  'post /pay/createCard': 'PayController.createCard',
  'post /pay/deleteCard': 'PayController.deleteCard',
  'get /pay/card': 'PayController.getCard',
  'get /pay/cards': 'PayController.getCards',
  'get /pay/plans': 'PayController.getPlans',
  'get /pay/plan': 'PayController.getPlan',
  'post /pay/plan': 'PayController.createPlan',
  'put /pay/plan': 'PayController.updatePlan',
  'get /pay/subscriptions': 'PayController.getSubscriptions',
  'get /pay/subscription': 'PayController.getSubscription',
  'post /pay/subscribeNew': 'PayController.subscribeNewCustomer',
  'post /pay/resubscribe': 'PayController.resubscribeCustomer',
  'post /pay/create/charge': 'PayController.createCharge',
  'post /pay/coupon': 'PayController.getCoupon',
  'post /pay/get_promo_amt': 'PayController.getPromoAmt',
  'post /pay/redeem_coupon': 'PayController.redeemCoupon',
  'put /pay/subscription': 'PayController.updateSubscription',
  'delete /pay/subscription': 'PayController.unsubscribe',
  'post /pay/refund_charge': 'PayController.refundCharge',

  // -- Nestcare API
  'post /nc/register': 'NestCareController.register',
  'post /nc/medications': 'NestCareController.medicationSchedules',
  'get /nc/vital_measurements': 'NestCareController.getVitalMeasurements',
  'get /nc/vital_schedules': 'NestCareController.getVitalSchedules',
  'get /nc/wellness_activities': 'NestCareController.getWellnessActivities',
  'get /nc/wellness_schedules': 'NestCareController.getWellnessSchedules',
  'get /nc/get_medication_activities': 'NestCareController.getMedicationActivities',
  'post /nc/doSso': 'NestCareController.doSso',
  'post /nc/changeAccountTypeAdmin': 'NestCareController.changeAccountTypeAdmin',
  'post /nc/updateShippingAdmin': 'NestCareController.updateShippingAdmin',
  'post /nc/resetPassword': 'NestCareController.resetPassword',
  'post /nc/ssoValidate': 'NestCareController.ssoValidate',
  'post /nc/getNonce': 'NestCareController.getNonce',
  'post /nc/login': 'NestCareController.login',
  'post /nc/adminLogin': 'NestCareController.adminLogin',
  'get /nc/admin_get_all_customers': 'NestCareController.adminGetAllNestcareCustomers',
  'get /nc/get_customer': 'NestCareController.getCustomer',
  'get /nc/admin_get_customer': 'NestCareController.adminGetCustomer',
  'get /nc/people_you_support': 'NestCareController.getPeopleYouSupport',
  'post /nc/2fa': 'NestCareController.twofactorVerify',
  'post /nc/updateProfile': 'NestCareController.updateProfile',
  'post /nc/updateShipping': 'NestCareController.updateShipping',
  'post /nc/updateBilling': 'NestCareController.updateBilling',
  'post /nc/add_payment_detail': 'NestCareController.addPaymentCustomerNumber',
  'post /nc/add_completed_order_tracker': 'NestCareController.addCompletedOrderTracker',
  'post /nc/insert_paid_user': 'NestCareController.insertPaidUser',
  'post /nc/remove_paid_user': 'NestCareController.removePaidUser',
  'post /nc/get_paying_user': 'NestCareController.getPayingUser',
  'post /nc/get_paid_for': 'NestCareController.getUsersPaidFor',
  'post /nc/change_account_type': 'NestCareController.changeAccountType',
  'post /nc/check_if_authorized': 'NestCareController.checkIfAuthorized',
  'post /nc/update_shipping_admin_as_auth_user': 'NestCareController.updateShippingAdminAsAuthorizedUser',
  'get /nc/view_completed_orders': 'NestCareController.viewCompletedOrdersHistory',
  'post /nc/admin_update_user_details': 'NestCareController.adminUpdateUserDetails',
  'post /nc/admin_change_verified': 'NestCareController.adminUpdateAccountVerified',
  // -- GoShippo API
  'post /ship/validate': 'ShippoController.verifyAddress',
  'post /ship/shipment': 'ShippoController.createShipment',
  'post /ship/refund': 'ShippoController.createRefund',
  'post /ship/label': 'ShippoController.createLabel',
  'get /ship/track': 'ShippoController.trackPackage',

  //twilio api

  'post /twilio/send_random': 'TwilioController.sendRandomNumberVerify',

  // -- discourse api.
  'post /dc/logout': 'discourseController.ssoLogout',
  /***************************************************************************
  *                                                                          *
  * Make the view located at `views/homepage.ejs` (or `views/homepage.jade`, *
  * etc. depending on your default view engine) your home page.              *
  *                                                                          *
  * (Alternatively, remove this and add an `index.html` file in your         *
  * `assets` directory)                                                      *
  *                                                                          *
  ***************************************************************************/

  '/': {
    view: 'homepage'
  }

  /***************************************************************************
  *                                                                          *
  * Custom routes here...                                                    *
  *                                                                          *
  * If a request to a URL doesn't match any of the custom routes above, it   *
  * is matched against Sails route blueprints. See `config/blueprints.js`    *
  * for configuration options and examples.                                  *
  *                                                                          *
  ***************************************************************************/

};
